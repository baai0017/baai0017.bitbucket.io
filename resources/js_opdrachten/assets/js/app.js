//1. Variabelen en array’s
let timeTableMaxVerstappen = [1.34, 1.45, 1.33, 1.44, 1.66];
console.log(timeTableMaxVerstappen[2]);

// 2. Functions
// Opdrachten
// 1. Maak een function (addNumbers) waarmee je twee getallen bij elkaar op telt.

const addNumbers = (numbOne, numbTwo) =
>
{
    return numbOne + numbTwo;
}

console.log(addNumbers(1, 1));

// 2. Maak een function (returnMiddleElement) die een array als parameter accepteert en
// vervolgens het middelste item in de array retourneert. Hou er rekening mee dat alleen een
// array met oneven elementen een middelste element heeft.

let numberArray = [1, 2, 3, 4, 5, 6, 7];

const returnMiddleElement = (array) =
>
{

    if (array.length % 2 === 0) {
        return 'The elements in the array are even';
    } else {
        return array[Math.floor(array.length / 2)];
    }

}
;

let result = returnMiddleElement(numberArray);
console.log(result);

//3. DOM manipulation
const inputFieldValue = document.querySelector('.time');
const submitButton = document.querySelector('.submit-button');
const timeTable = document.querySelector('.max-verstappen-time-table tbody');

const addTimeToTimeTable = () =
>
{

    //Haal de value uit de input veld en sla het op in de value variable
    let value = document.querySelector('.form-result').innerHTML = inputFieldValue.value;

    //Maak een DOM <tr> tag aan
    let tr = document.createElement('tr');

    //Maak een DOM <td> tag aan
    let td = document.createElement('td');

    //Zet de tekst klaar om het tussen een td tag te plaatsen
    let textNode = document.createTextNode(value);

    tr.appendChild(td);
    td.appendChild(textNode);
    timeTable.appendChild(tr);
}
;

// 4. Loops
// Bekijk het volgende onderdeel van de JS cursus: ‘9. Loops’.
// Opdrachten
// 1. Gegeven de array van Max Verstappen uit opdracht 1.1. Maak een functie die het gemiddelde
// van de rondetijden berekent.
// 2. Zorg er voor dat de gemiddelde rondetijd onderaan de tabel komt te staan. Dit moet
// dynamisch met JS gebeuren.
// 3. In opdracht 3.1 heb je de rondetijden van Max Verstappen zelf in de HTML gezet. Zorg ervoor
// dat dit vanuit JS wordt gedaan. Dus wanneer je de pagina opent worden de rondetijden
// dynamisch met JavaScript op de pagina getoond. Let er wel op dat opdracht 3.2 gewoon blijft
// werken.


const average = (array) =
>
{
    let arrayLength = array.length;
    let totalTime = 0;

    for (let i = 0; i < arrayLength; i++) {
        totalTime += array[i];
    }

    let result = totalTime / arrayLength;
    let roundResult = result.toFixed(2);

    //Haal de value uit de input veld en sla het op in de value variable
    let value = roundResult;

    //Maak een DOM <tr> tag aan
    let tr = document.createElement('tr');

    //Maak een DOM <td> tag aan
    let td = document.createElement('td');

    //Zet de tekst klaar om het tussen een td tag te plaatsen
    let textNode = document.createTextNode(value);

    tr.appendChild(td);
    td.appendChild(textNode);
    timeTable.appendChild(tr);

    return roundResult;
}

average(timeTableMaxVerstappen);

