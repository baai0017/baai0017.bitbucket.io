let pictureArray = ['dog', 'mouse', 'cat', 'frog', 'bug', 'fly', 'spider', 'bat', 'monkey'];

let timeSelector = document.querySelector('#time');
let button = document.querySelector('.button');

let points = 0;
let timer = [0, 0, 0, 0];
let timeRunning = false;
let interval;
let animalFound = false;

let audioYay = new Audio('libs/audio/yay.mp3');
let audioWhoo = new Audio('libs/audio/whoo.mp3');

const init = () => {
    //define random animal.
    let randomAnimal = getRandomAnimal(pictureArray);

    //create en place gui in .html
    postImages(pictureArray, '.jpg', '.img-grid');

    //get one random animal from the array
    placeRandomImage(pictureArray, '.picture', '.jpg', randomAnimal);

    //check if user is correct / image equal to an other image
    listenToLinks('.img-grid', randomAnimal);

    //score init
    score();

    startTimer();
};

window.addEventListener("load", init);

const playAudio = () => {
    audioYay.play();
    audioWhoo.play();
};

const reset = () => {
    resetTimer();
    resetGame();
    init();
};

const resetTimer = () => {
    clearInterval(interval);
    interval = 0;
    timer = [0, 0, 0, 0];
    timeRunning = false;
};

const leadingZero = (time) => {
    if (time <= 9) {
        time = "0" + time;
    }
    return time;
};

const runTimer = () => {
    let currentTime = leadingZero(timer[0]) + ":" + leadingZero(timer[1]) + ":" + leadingZero(timer[2]);
    timeSelector.innerHTML = currentTime;
    timer[3]++;

    timer[0] = Math.floor((timer[3] / 100) / 60);
    timer[1] = Math.floor((timer[3] / 100) - (timer[0] * 60));
    timer[2] = Math.floor(timer[3] - (timer[1] * 100) - (timer[0] * 6000));
};

const startTimer = () => {
    if (document.readyState === "complete" && !timeRunning) {
        timeRunning = true;
        interval = setInterval(runTimer, 10);
    }
};

const resetGame = () => {
    resetPlaceRandomImage('.picture');
    removeChildsInsideParent('.img-grid');
    animalFound = false;
};

const score = (winOrLose) => {
    //Define where points need to be show in GUI
    let selector = document.querySelector('#points');

    if (winOrLose === true) {
        //the user won the game so he/she gets a point.
        points += 1;
    } else if (winOrLose === false) {

        //The user lost a game so he/she loses a game, if the score is above 0.
        if (points > 0) {
            points -= 1;
        }

    } else {
        //Reset score to 0
        selector.innerHTML = '0';
    }
    //Update score in GUI
    selector.innerHTML = `${points}`;
};

const listenToLinks = (selector, randomAnimal) => {
    //Define main selection images
    selector = document.querySelector(selector);

    //Get all links from images
    let linkArray = selector.children;

    //Loop trough all links and images
    for (var i = 0; i < linkArray.length; i++) {

        //When user clicks on a link from a image call function
        linkArray[i].addEventListener('click', function (event) {

            //Check if image is equal to other image
            if (randomAnimal == event.target.alt && !animalFound) {

                animalFound = true;

                //play audio file
                playAudio();

                //Send a alert message to the user
                alert("Correct!");

                //Keep score
                score(true);

                //Stop timer
                clearInterval(interval);

                // If user clicks on the button, he/she creates a new game.
                button.addEventListener('click', reset, false);

                //If user presses the spacebar he/she creates a new game.
                document.body.onkeyup = function (e) {
                    if (e.keyCode == 32) {
                        reset();
                    }
                }

            } else if (!animalFound) {
                alert("Fout, probeer het opnieuw");

                //Keep score
                score(false);

            } else {
                return null;
            }

        }, false);

    }
};

//Get random element out of array
const getRandomAnimal = (array) => {
    return array[Math.floor(Math.random() * array.length)];
};

//Reset random image
const resetPlaceRandomImage = (resetImageSelector) => {
    //reset random top image
    let randomImg = document.querySelector(resetImageSelector);
    randomImg.removeAttribute('src');
    randomImg.removeAttribute('alt');
    randomImg.removeAttribute('id');
};

//Get the random Ani
const placeRandomImage = (array, selector, imageFormat, randomAnimal) => {
    selector = document.querySelector(selector);

    selector.setAttribute('src', 'libs/img/' + randomAnimal + imageFormat);
    selector.setAttribute('alt', randomAnimal);
    selector.setAttribute('id', randomAnimal);
};

const removeChildsInsideParent = (selector) => {
    //reset image grid
    let parent = document.querySelector(selector);
    parent.innerHTML = '';
};

const postImages = (array, imageFormat, selector) => {
    selector = document.querySelector(selector);
    let shuffleArray = shuffle(array);

    //check if selector is empty
    for (let i = 0; i < shuffleArray.length; i++) {

        let a = document.createElement('a');
        let img = document.createElement('img');

        a.setAttribute('href', '#' + shuffleArray[i]);
        a.setAttribute('id', shuffleArray[i]);
        a.setAttribute('title', shuffleArray[i]);
        a.setAttribute('class', 'animal-img');

        img.setAttribute('src', 'libs/img/' + shuffleArray[i] + imageFormat);
        img.setAttribute('alt', shuffleArray[i]);
        img.setAttribute('class', 'imageRot');

        a.appendChild(img);
        selector.appendChild(a);
    }

};

//shuffle array
const shuffle = (array) => {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
};